public class Criniere{



    private Integer longueurCriniere ;
   private TEXTURE textureCriniere;
    //constructeur
    public Criniere(Integer longeurcriniere, TEXTURE texturecriniere) {
        this.longueurCriniere = longeurcriniere;
        this.textureCriniere = texturecriniere;
    }
    //Getter
    public Integer getLongueurCriniere() {
        return longueurCriniere;
    }

    public TEXTURE getTextureCriniere() {
        return textureCriniere;
    }

}
