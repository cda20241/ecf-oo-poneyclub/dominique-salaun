import java.util.Date;

public class LotFoin {

    private String numeroLotFoin;
    private Integer quantiteLotFoin;
    private TYPEFOIN typeFoin;
    private Salarier salarier;
    private Date dateDeLivraison;

//constructeur
    public LotFoin(String numeroLotFoin, Integer quantiteLotFoin,TYPEFOIN typeFoin,Salarier salarier,Date dateDeLivraison) {
        this.numeroLotFoin = numeroLotFoin;
        this.quantiteLotFoin = quantiteLotFoin;
        this.typeFoin = typeFoin;
        this.salarier = salarier;
        this.dateDeLivraison= dateDeLivraison;

    }
//Getter
    public TYPEFOIN getTypeFoin() {
        return typeFoin;
    }

    public String getNumeroLotFoin() {
        return numeroLotFoin;
    }

    public Integer getQuantiteLotFoin() {
        return quantiteLotFoin;
    }

    public Salarier getSalarier() {
        return salarier;
    }

    public Date getDateDeLivraison() {
        return dateDeLivraison;
    }
}
