import java.util.ArrayList;
import java.util.List;

public class Ecurie {


    public static String nomEcurie;
    private Integer tailleBox;
    private static List<String> listEcuries = new ArrayList<>();
    private List<Salarier> listSalarier;
    private List<Box> listBox;
    private List<Poney> listponey;
    private List<Box> listpaitre;

//Constructeur
    public Ecurie(String nomEcurie) {
        this.nomEcurie = nomEcurie;
        this.listSalarier =  new ArrayList<>();
        this.listBox = new ArrayList<>();
        this.listponey =  new ArrayList<>();
        this.listpaitre = new ArrayList<>();
        ajouterEcurieList(nomEcurie);
    }


    /**
     * Cette Methode permet d'ajouter lecurie creer dans une listecuries en prenant en parametre :
     *
     * @param nomEcurie
     */
    public void ajouterEcurieList(String nomEcurie){
    listEcuries.add(nomEcurie);
    }



    /**
     * Cette Methode permet de lister les ecuries dans la liste listecuries sans parametre :
     *
     */
        public static void listerEcuries(){
        System.out.println("\nListe des Ecuries actuel : ");
        if(listEcuries.isEmpty()){System.out.println("Aucune Ecurie Referencer dans la liste des Ecuries");}
        else{
        for(String ecurie :listEcuries){
            System.out.println(ecurie);
        }
        }
        System.out.println("");
    }

    /**
     * Cette methode fait une boucle for prennant  La listBox pour donner le pourcentage de remplissage de chqaue box
     */
    public void calculerPourcentage() {
        // Calculer le pourcentage de remplissage pour chaque box
        for (Box box : listBox) {
            float pourcentageRemplissage = (box.getQuantiteFoinCourante() * 100) / box.getQuantiteFoinMax();
            System.out.println("Pourcentage de remplissage pour le box " + box.getNomBox() + " : " + pourcentageRemplissage + "%");
        }
    }


    /**
     * cette methode permet de calculer et repartir equitablement en pourcentage chaque box par rapport
     * a la quantitéCourante, la quantitémax et le nombre de box
     *
     * @param livraison
     */
    public void repartirFoin(Livraison livraison) {
        Integer quantiteFoinLivrer = livraison.getLotFoin().getQuantiteLotFoin();
        //je dois faire le total de tout les quantitemax des boxs
        int quantiteFoinMaxTotal = 0;
        for (Box box : listBox) {
            quantiteFoinMaxTotal += box.getQuantiteFoinMax();
        }

    //je dois faire le total de tout les quantiteCourante des boxs
        int quantiteFoinCourantTotal = 0;
        for (Box box : listBox) {
            quantiteFoinCourantTotal += box.getQuantiteFoinCourante();
        }
        // je fait le total de quantiteenstock en addictionent Totalcourant + quantite livré
        Integer quantiteEnStock = quantiteFoinLivrer+ quantiteFoinCourantTotal;

        float totalMaxDiviser = (float) (quantiteFoinMaxTotal/100);

        float pourcentageStock = quantiteEnStock / totalMaxDiviser;

        for (Box box :listBox){
            float parbox = box.getQuantiteFoinMax()/100;

            float courantBox = pourcentageStock * parbox;
            box.setQuantiteFoinCourante(courantBox);

        }
        }






    // Méthode pour mettre à jour le pourcentage équilibré après l'ajout ou la suppression d'un box


//methode box

    /**
     * Cette Methode permet de creer un box en prenant en parametre :
     *
     * @param nomTextBoxCreer
     * @param tailleBox
     * @param quantitéMaxFoin
     * @param quantitéCouranteFoin
     *
     * et lajouter a la liste Box
     */
    public void ajouterBoxEcurie(String nomTextBoxCreer,Integer tailleBox,Integer quantitéMaxFoin,float quantitéCouranteFoin){
    Box boxEcurie = new Box(nomTextBoxCreer,tailleBox,quantitéMaxFoin,quantitéCouranteFoin);
    this.listBox.add(boxEcurie);
    System.out.println(boxEcurie.getNomBox()+" Ajouter a l'ecurie "+nomEcurie);
    }


    /**
     * Cette Methode permet de  lister les boxs et retourne un message si pas de box
     *
     */
    public void listerBoxEcurie(){
        System.out.println("\nListe Des Box de " + nomEcurie + " :");
        if (listBox.isEmpty()) {
            System.out.println("Il n'y a aucun box actuellement");
        } else {
            for (Box box : listBox) {
                System.out.println(box.getNomBox()+ " de taille "+box.getTailleBox()+" avec actuellement "+box.getQuantiteFoinCourante() +" Kilo restant sur "+ box.quantiteFoinMax +" Max." );
            }
        }
        System.out.println("");
    }




//Methode Salarier
    /**
     * Cette Methode permet de recruter un salarier en prenant en parametre :
     *
     * @param nomPersonne
     * @param prenomPersonne
     * @param fonctionDuSalarie
     *
     * et l'ajout du salarier a la list Salarier
     */
    public void recruterSalarier(String nomPersonne, String prenomPersonne, String fonctionDuSalarie){
    Salarier salarier = new Salarier(nomPersonne,prenomPersonne,fonctionDuSalarie);
    this.listSalarier.add(salarier);
    System.out.println(salarier.getNomPersonne()+" "+salarier.getPrenomPersonne()+" a été emabauché à l'écurie "+nomEcurie+" au poste de "+fonctionDuSalarie);
    }


    /**
     * Cette Methode permet de  licencier un salarier en prenant en parametre :
     *
     * @param nomPersonne
     * @param prenomPersonne
     *
     * verifira si les parametre sont dans la list salarier
     * et la suppression du salarier a la list Salarier
     */
    public void licencierSalarier(String nomPersonne, String prenomPersonne){
    for (Salarier employer : listSalarier ){
    if(employer.getNomPersonne().toLowerCase().equals(nomPersonne.toLowerCase()) && employer.getPrenomPersonne().toLowerCase().equals(prenomPersonne.toLowerCase()) ){
        this.listSalarier.remove(employer);
        System.out.println(employer.getNomPersonne()+" "+employer.getPrenomPersonne()+" a été licencié de l'ecurie "+nomEcurie);
        return;
    }
    else{System.out.println("Il n'y a pas de salarié de se nom dans l'ecurie");}
}
        System.out.println("");
    }

    /**
     * Cette Methode permet de  lister la list des salarier et retourne un message si pas de salarier
     *
     */
    public void listerSalarier() {
        System.out.println("\nListe Des Salariés de " + nomEcurie + " :");

        if (listSalarier.isEmpty()) {
            System.out.println("Il n'y a aucun salarié actuellement");
        } else {
            for (Salarier employer : listSalarier) {
                System.out.println(employer.getPrenomPersonne() + " " + employer.getNomPersonne() + " " + employer.getFonctionDuSalarie());
            }
        }

        System.out.println("");
    }



//poney

    /**
     * Cette methode permet de mettre un poney a paitre
     * @param poney
     * @param box
     */
    public void mettrePoneyPaitre(Poney poney, Box box){
        if (!box.getListPoney().contains(poney)) {
            System.out.println("Le poney " + poney.getNomPoney() + " n'est pas dans le box " + box.getNomBox());
            return; // Sortir de la méthode si le poney n'est pas dans la boîte source
        }
        for (Box boxTemp :listBox)
        {
            if(boxTemp.equals(box)){
                boxTemp.getListPoney().remove(poney);
                for (Box paitreTemp :listpaitre){
                    paitreTemp.getListPoney().add(poney);
                }
            }
        }
        System.out.println("Sorti de "+poney.getNomPoney()+" du " +box.getNomBox()+" pour aller paitre\n");

    }

    /**
     * cettre methode permet de rentrer un poney aui etait a paitre
     *
     * @param poney
     * @param box
     */
    public void rentrerPoneyPaitre(Poney poney, Box box){

        for (Box paitreTemp :listpaitre)
        {
            if(paitreTemp.equals(box)){
                paitreTemp.getListPoney().add(poney);
                for (Box boxTemp :listBox){
                    boxTemp.getListPoney().add(poney);
                }
            }
        }
        System.out.println("retour de "+poney.getNomPoney()+" au " +box.getNomBox()+" apres etre allé paitre. \n");
    }


    /**
     * Cette methode permet dajouter un poney dans un box
     * @param poney
     * @param box
     */
    public void entrerPoneyBox(Poney poney, Box box){

           for (Box boxTemp :listBox)
           {
               if(boxTemp.equals(box)){
                   boxTemp.getListPoney().add(poney);
               }
                    }
               System.out.println("Ajoute de : "+poney.getNomPoney()+" en " +box.getNomBox());
        }


    /**
     * cette methode permet de sortir un poney de son box
     * @param poney
     * @param box
     */
    public void sortirPoneyBox(Poney poney, Box box){
        if (!box.getListPoney().contains(poney)) {
            System.out.println("Le poney " + poney.getNomPoney() + " n'est pas dans le box " + box.getNomBox());
            return; // Sortir de la méthode si le poney n'est pas dans la boîte source
        }
        for (Box boxTemp :listBox)
        {
            if(boxTemp.equals(box)){
                boxTemp.getListPoney().remove(poney);
            }
        }

        System.out.println("suppression de :"+poney.getNomPoney()+" du " +box.getNomBox());
    }

    /**
     *  Cette methode automatiser permet de changer un poney d'un box vers une autre box
     * @param poney
     * @param boxSource
     * @param boxDestination
     */

    public void changerPoneyBoxToBox(Poney poney, Box boxSource, Box boxDestination) {

        // Vérifier si le poney est dans la boîte source
        if (!boxSource.getListPoney().contains(poney)) {
            System.out.println("Le poney " + poney.getNomPoney() + " n'est pas dans le box " + boxSource.getNomBox());
            return; // Sortir de la méthode si le poney n'est pas dans la boîte source
        }

        // Retirer le poney de la boîte source
        for (Box boxTempSource : listBox) {
            if (boxTempSource.equals(boxSource)) {
                boxTempSource.getListPoney().remove(poney);
                System.out.println(poney.getNomPoney() + " sort du " + boxTempSource.getNomBox());
                break; // Sortir de la boucle après avoir trouvé la boîte source
            }
        }

        // Ajouter le poney à la boîte de destination
        for (Box boxTempDestination : listBox) {
            if (boxTempDestination.equals(boxDestination)) {
                boxTempDestination.getListPoney().add(poney);
                System.out.println(poney.getNomPoney() + " entre dans le " + boxTempDestination.getNomBox());
                break; // Sortir de la boucle après avoir trouvé la boîte de destination
            }
        }

        // Afficher le message de transfert
        System.out.println("Le poney " + poney.getNomPoney() + " a bien été transféré de "
                + boxSource.getNomBox() + " vers " + boxDestination.getNomBox());
    }

//Getter
    public List<Salarier> getListSalarier() {
        return listSalarier;
    }

    public List<Box> getListBox() {
        return listBox;
    }

}






