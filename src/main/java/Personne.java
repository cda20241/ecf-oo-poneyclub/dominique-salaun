public class Personne {
    private String NomPersonne;
    private String PrenomPersonne;

//Constructeur
    public Personne(String nomPersonne, String prenomPersonne) {
        this.NomPersonne = nomPersonne;
        this.PrenomPersonne = prenomPersonne;
    }
//Getter
    public String getNomPersonne() {
        return NomPersonne;
    }

    public String getPrenomPersonne() {
        return PrenomPersonne;
    }

}
