import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Ecurie e1 = new Ecurie("Tatooine Poney Club");
        Ecurie.listerEcuries();
        e1.ajouterBoxEcurie("Box A",3,600, 200);
        e1.ajouterBoxEcurie("Box B",4,600,300);
        e1.ajouterBoxEcurie("Box C",5,1000,900);
        e1.ajouterBoxEcurie("Box D",6,800,100);
        e1.listerBoxEcurie();
        e1.calculerPourcentage();

        e1.recruterSalarier("Dark","VADOR","Palfrenier");
        e1.licencierSalarier("Dark","VADOR");
        e1.recruterSalarier("Obiwan","KENOBI","Palfrenier");
        e1.recruterSalarier("Luc","SKYWALKER","Palfrenier");
        e1.listerSalarier();

        Poney p1 = new Poney("Usain Bolt",250,25,TEXTURE.LISSE,false) {};
        Poney p2 =  new Poney("Petit Tonnerre",280,28,TEXTURE.FRISEE,false) {};

        e1.entrerPoneyBox(p1,e1.getListBox().get(0));
        e1.entrerPoneyBox(p2,e1.getListBox().get(1));

        e1.changerPoneyBoxToBox(p1,e1.getListBox().get(0),e1.getListBox().get(3));
        e1.changerPoneyBoxToBox(p1,e1.getListBox().get(0),e1.getListBox().get(3));
        e1.changerPoneyBoxToBox(p1,e1.getListBox().get(3),e1.getListBox().get(0));

        e1.mettrePoneyPaitre(p1,e1.getListBox().get(0));
        e1.rentrerPoneyPaitre(p1,e1.getListBox().get(0));


        Salarier luc = e1.getListSalarier().get(1);
        Livraison livraison = new Livraison(luc,new Date(),"F003",200,TYPEFOIN.FOINDEPRAIRIE);
        livraison.dateReceptionLot(luc,new Date());
        livraison.receptionLot(new Date());
        livraison.listerlivraison(luc);

        Livraison livraison1 = new Livraison(luc,new Date(),"F052",300,TYPEFOIN.FOINDEGRAMINEES);
        livraison1.dateReceptionLot(luc,new Date());
        livraison1.receptionLot(new Date());
        livraison1.listerlivraison(luc);

        e1.listerBoxEcurie();
        e1.repartirFoin(livraison);
        e1.listerBoxEcurie();
        e1.calculerPourcentage();



    }
}
