public enum TYPEFOIN {
    /**
     * Enum pour le type de foin
     */
    FOINDEPRAIRIE("Foin de Prairie"),
    FOINDEGRAMINEES("Foin de Graminées");

    public String Text;

    //Constructeur
    TYPEFOIN(String text) {
        Text = text;
    }

    /**
     * Getter
     * @return
     */
    public String getText() {
        return Text;
    }

    /**
     * Setter
     * @param text
     */
    public void setText(String text) {
        Text = text;
    }
}
