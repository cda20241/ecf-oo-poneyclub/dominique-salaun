import java.util.ArrayList;
import java.util.List;

public class Box {
private String nomBox;
    private Integer tailleBox;
    private float quantiteFoinCourante;
    Integer quantiteFoinMax;
    private List<Poney> listPoney;
    private Integer poidsFoin;


    //Constructeur
    public Box(String nomBox, Integer tailleBox, Integer quantiteFoinMax, float quantiteFoinCourante) {
        this.nomBox=nomBox;
        this.tailleBox = tailleBox;
        this.quantiteFoinCourante = quantiteFoinCourante;
        this.quantiteFoinMax = quantiteFoinMax;
        this.listPoney = new ArrayList<>();

    }
    //Getter
    public String getNomBox() {
        return nomBox;
    }

    public Integer getTailleBox() {
        return tailleBox;
    }

    public float getQuantiteFoinCourante() {
        return quantiteFoinCourante;
    }

    public Integer getQuantiteFoinMax() {
        return quantiteFoinMax;
    }

    public List<Poney> getListPoney() {
        return listPoney;
    }

    //Setter
    public void setQuantiteFoinCourante(float quantiteFoinCourante) {
        this.quantiteFoinCourante = quantiteFoinCourante;
    }



}
