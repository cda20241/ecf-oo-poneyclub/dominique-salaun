public abstract class Poney {

    private String ecurie = Ecurie.nomEcurie;
    private String nomPoney;
    private Integer poidPoney;
    private Criniere criniere;
    private Boolean paitre = false;
//Constructeur
    public Poney(String nomPoney, Integer poidPoney, Integer longueurCriniere, TEXTURE texturecriniere, Boolean paitre) {
        this.nomPoney = nomPoney;
        this.poidPoney = poidPoney;
        criniere = new Criniere( longueurCriniere,texturecriniere);
        this.paitre = paitre;
        System.out.println("Le Poney "+nomPoney+" de "+ poidPoney+" Kilos, avec une longueur de "+criniere.getLongueurCriniere()+" Cm de crenière, et de texture "+criniere.getTextureCriniere()+" a rejoint "+ecurie );

    }


//Getter
    public String getNomPoney() {
        return nomPoney;
    }



}
