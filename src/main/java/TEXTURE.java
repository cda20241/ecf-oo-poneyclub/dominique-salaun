public enum TEXTURE {

    /**
     * Enum pour la criniere du poney
     */
    FRISEE("Frisée"),
    LISSE("Lisse");


    //Constructeur
    TEXTURE(String text) {
        this.text = text;
    }

    //Attribu
    public String text ;

    /**
     *     Getter
     * @return
     */
    public String getText() {
        return text;
    }

    /**
     * Setter
     * @param text
     */
    public void setText(String text) {
        this.text = text;
    }








}
