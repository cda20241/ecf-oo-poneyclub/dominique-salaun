public class Salarier extends Personne{
    private String fonctionDuSalarie;

//Constructeur
    public Salarier(String nomPersonne, String prenomPersonne,String fonctionDuSalarie ) {
        super(nomPersonne, prenomPersonne);
        this.fonctionDuSalarie = fonctionDuSalarie;
    }

//Getter
    public String getFonctionDuSalarie() {
        return fonctionDuSalarie;
    }


}
