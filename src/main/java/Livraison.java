import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Livraison {
    private Date dateReception;
   // private Date dateDeLivraison;
    // private List<Date> dateDeLivraison = new ArrayList<>();
    private List<LotFoin> listLivraisonFoin =new ArrayList<LotFoin>();
    private LotFoin lotFoin;
   //Construsteur
    public Livraison( Salarier salarier, Date dateDeLivraison,String numeroLotFoin, Integer quantiteLotFoin, TYPEFOIN typeFoin) {

        lotFoin = new LotFoin(numeroLotFoin,quantiteLotFoin,typeFoin,salarier,dateDeLivraison);

    }


    /**
     * cette methode permet de receptionner un lot de foin
     */
    public void receptionLot(Date dateDeLivraison) {
        if (dateDeLivraison == null) {
            System.out.println("Le lot n'a pas encore été livré.");
            return;
        }

        // Traitement de la réception du lot
        System.out.println("Lot réceptionné Numero : " + this.lotFoin.getNumeroLotFoin()+", Poid : "+ this.lotFoin.getQuantiteLotFoin()+"Kilos, type de foin : "+this.lotFoin.getTypeFoin());
        listLivraisonFoin.add(lotFoin);
    }

    /**
     * cette methode prend le salarier actuel et indiaue la date de reception
     * @param salarier
     */
    public void dateReceptionLot(Salarier salarier, Date dateReception ) {
        System.out.println("Date de réception du lot : " + dateReception);
        System.out.println("Lot réceptionné par : " + salarier.getNomPersonne() + " " + salarier.getPrenomPersonne());
    }
    public void listerlivraison(Salarier salarier) {
        System.out.println("\nListe Des Livraisons :");
        if (listLivraisonFoin.isEmpty()) {
            System.out.println("Il n'y a aucune livraison actuellement ");
        } else {
            for (LotFoin recepTemp : listLivraisonFoin) {
                System.out.println("Lot : " + recepTemp.getNumeroLotFoin() +
                        ", Poids : " + recepTemp.getQuantiteLotFoin() +
                        " Kilos, Type : " + recepTemp.getTypeFoin() +
                        ", Par : " + recepTemp.getSalarier().getNomPersonne() +
                        " " + recepTemp.getSalarier().getPrenomPersonne() +
                        ", Receptionné le : " + recepTemp.getDateDeLivraison());
            }
        }
        System.out.println("");
    }
//Getter
    public LotFoin getLotFoin() {
        return lotFoin;
    }
}
